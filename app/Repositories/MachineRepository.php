<?php

namespace App\Repositories;

use App\Exceptions\MachineIsAlreadyReservedException;
use App\Models\Machine;
use App\Models\MachineReservation;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class MachineRepository
 * @package App\Repositories
 */
class MachineRepository extends Repository
{
    /**
     * @var MachineReservation
     */
    protected $machineReservation;

    /**
     * MachineRepository constructor.
     *
     * @param Machine $machine
     * @param MachineReservation $machineReservation
     */
    public function __construct(Machine $machine, MachineReservation $machineReservation)
    {
        parent::__construct($machine);

        $this->machineReservation = $machineReservation;
    }

    /**
     * @param array $inDays
     *
     * @return Collection
     */
    public function getReservedInDays(array $inDays)
    {
        return $this->machineReservation
            ->whereIn('day_of_week', $inDays)
            ->orderBy('start_time')
            ->get();
    }

    /**
     * @param Machine $machine
     * @param $dayOfWeek
     *
     * @throws MachineIsAlreadyReservedException
     */
    public function checkIfMachineIsAvailableAtDay(Machine $machine, $dayOfWeek)
    {
        if($this->isMachineAvailableAtDay($machine, $dayOfWeek) === false) {
            throw new MachineIsAlreadyReservedException();
        }
    }

    /**
     * @param Machine $machine
     * @param $dayOfWeek
     *
     * @return bool
     */
    public function isMachineAvailableAtDay(Machine $machine, $dayOfWeek)
    {
        return $machine->reservations()->where('day_of_week', '=', $dayOfWeek)->exists() === false;
    }
}
<?php

namespace App\Exceptions;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /**
         * @var Response $response
         */
        $response = app(Response::class);

        switch (true) {
            case $exception instanceof MachineIsAlreadyReservedException:
                return $response->setStatusCode($exception->getCode())
                    ->withError($exception->getMessage(), $exception->getCode());

            case $exception instanceof ModelNotFoundException:
                return $response->errorNotFound('Entity was not found.');

            case $exception instanceof NotFoundHttpException:
                return $response->errorNotFound('Endpoint does not exists.');

            case $exception instanceof ValidationException:
                $errors = $exception->validator->errors();

                return $response->errorWrongArgs($errors->messages());
        }

        return parent::render($request, $exception);
    }

    /**
     * @inheritdoc
     */
    protected function prepareResponse($request, Exception $e)
    {
        return $this->prepareJsonResponse($request, $e);
    }
}

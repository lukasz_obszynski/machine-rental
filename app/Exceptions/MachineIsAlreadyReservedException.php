<?php

namespace App\Exceptions;
use Illuminate\Http\Response;
use Throwable;

/**
 * Class MachineIsAlreadyReservedException
 * @package App\Exceptions
 */
class MachineIsAlreadyReservedException extends \Exception {
    /**
     * MachineIsAlreadyReservedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        $message = 'Machine is already reserved for given day.',
        $code = Response::HTTP_CONFLICT,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Machine
 * @package App\Models
 *
 * @property int $id
 * @property int $machine_id
 * @property string $start_time
 * @property string $end_time
 * @property int $day_of_week
 */
class MachineReservation extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'machine_reservation';

    /**
     * @inheritdoc
     */
    public $timestamps = false;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'start_time',
        'end_time',
        'day_of_week'
    ];

    /**
     * @return BelongsTo
     */
    public function machine()
    {
        return $this->belongsTo(Machine::class, 'machine_id', 'id');
    }
}

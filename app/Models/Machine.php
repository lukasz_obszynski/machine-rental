<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Machine
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 */
class Machine extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'machine';

    /**
     * @inheritdoc
     */
    protected $fillable = ['name'];

    /**
     * @return HasMany
     */
    public function reservations()
    {
        return $this->hasMany(MachineReservation::class, 'machine_id');
    }
}
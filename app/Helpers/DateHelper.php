<?php

namespace App\Helpers;

/**
 * Class DateHelper
 */
class DateHelper
{
    const DATE_DEFAULT_FORMAT = 'Y-m-d';
    const TIME_DEFAULT_FORMAT = 'G:i';

    const MINUTE = 60;
    const HOUR = self::MINUTE * 60;
    const DAY = self::HOUR * 24;

    /**
     * @param string $startDate
     * @param int $days
     *
     * @return array
     */
    public static function getArrayOfDOWFromDateForDays($startDate, $days)
    {
        if ($days >= 7) {
            return range(0, 6);
        }

        $dow = self::getDow($startDate);
        $daysArr = [$dow];

        if ($days <= 1) {
            return $daysArr;
        }

        for ($i = $dow + 1; $i < $days + $dow; ++$i) {
            $daysArr[] = $i % 7;
        }

        return $daysArr;
    }

    /**
     * @param string $date
     *
     * @return false|string
     */
    public static function getDow($date)
    {
        $dow = date('w', strtotime($date));

        return $dow ? intval($dow) : false;
    }

    /**
     * @param string $date
     * @param int $days
     *
     * @return false|string
     */
    public static function increaseByDays($date, $days)
    {
        $time = strtotime($date);

        return date(self::DATE_DEFAULT_FORMAT, $time + $days * self::DAY);
    }

    /**
     * @param int $dow
     *
     * @return string|null
     */
    public static function getTextOfDOW($dow)
    {
        $map = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ];

        return $map[$dow] ?? null;
    }
}
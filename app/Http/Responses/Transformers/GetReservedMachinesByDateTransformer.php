<?php

namespace App\Http\Responses\Transformers;

use App\Helpers\DateHelper;
use App\Models\MachineReservation;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class GetReservedMachinesByDateTransformer
 * @package App\Http\Responses\Transformers
 */
class GetReservedMachinesByDateTransformer extends TransformerAbstract
{
    /**
     * @var string
     */
    protected $startDate;

    /**
     * @var int
     */
    protected $forDays;

    /**
     * GetReservedMachinesByDateTransformer constructor.
     *
     * @param string $startDate
     * @param int $forDays
     */
    public function __construct($startDate, $forDays)
    {
        $this->startDate = $startDate;
        $this->forDays = intval($forDays);
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    public function transform(Collection $collection)
    {
        $grouped = $collection->mapToGroups(function (MachineReservation $item) {
            return [$item->day_of_week => $item];
        });

        return $this->prepareDays($grouped);
    }

    /**
     * @param Collection $collection
     *
     * @return array
     */
    protected function prepareDays(Collection $collection)
    {
        $dayOfWeek = DateHelper::getDow($this->startDate);
        $result = [];

        for ($i = $dayOfWeek; $i < $dayOfWeek + $this->forDays; ++$i) {
            $key = $i % 7;

            $result[] = $this->prepareDay($collection->get($key), $i - $dayOfWeek);
        }

        return $result;
    }

    /**
     * @param Collection|null $collection
     * @param int $dayCounter
     *
     * @return array
     */
    protected function prepareDay(Collection $collection = null, $dayCounter)
    {
        $arr = [
            'date' => DateHelper::increaseByDays($this->startDate, $dayCounter),
            'hours' => []
        ];

        if ($collection !== null) {
            $collection->each(function (MachineReservation $item) use (&$arr) {
                $arr['hours'][] = [$item->start_time, $item->end_time];
            });
        }

        $arr['hours'] = $this->mergeOverlapedHours($arr['hours']);

        return $arr;
    }

    /**
     * @param array $hours
     *
     * @return array
     */
    protected function mergeOverlapedHours(array $hours)
    {
        $groupKey = null;

        foreach ($hours as $key => list($startTime, $endTime)) {
            if ($groupKey === null) {
                $groupKey = $key;

                continue;
            }

            list($lastST, $lastET) = $hours[$groupKey];
            if ($startTime > $lastET) {
                $groupKey = $key;
            }

            $hours[$groupKey] = [$lastST, max($lastET, $endTime)];
            unset($hours[$key]);
        }

        return $hours;
    }
}
<?php

namespace App\Http\Responses\Transformers;

use App\Helpers\DateHelper;
use App\Models\MachineReservation;
use League\Fractal\TransformerAbstract;

/**
 * Class MachineReservationTransformer
 * @package App\Http\Responses\Transformers
 */
class MachineReservationTransformer extends TransformerAbstract
{
    /**
     * @param MachineReservation $machineReservation
     *
     * @return array
     */
    public function transform(MachineReservation $machineReservation)
    {
        return [
            'start_time' => $machineReservation->start_time,
            'end_time' => $machineReservation->end_time,
            'day_of_week' => DateHelper::getTextOfDOW($machineReservation->day_of_week),
        ];
    }
}
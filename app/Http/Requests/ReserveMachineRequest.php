<?php

namespace App\Http\Requests;

use App\Helpers\DateHelper;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReserveMachineRequest
 * @package App\Http\Requests
 */
class ReserveMachineRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'day_of_week' => 'required|integer|min:0|max:6',
            'start_time' => 'required|date_format:' . DateHelper::TIME_DEFAULT_FORMAT,
            'end_time' => 'required|date_format:' . DateHelper::TIME_DEFAULT_FORMAT,
        ];
    }
}
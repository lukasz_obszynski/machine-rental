<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ReserveMachineRequest
 * @package App\Http\Requests
 */
class GetReservedMachinesByDateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|date',
            'days' => 'required|integer',
        ];
    }
}
<?php

namespace App\Http\Controllers;

use App\Exceptions\MachineIsAlreadyReservedException;
use App\Helpers\DateHelper;
use App\Http\Requests\GetReservedMachinesByDateRequest;
use App\Http\Requests\ReserveMachineRequest;
use App\Http\Responses\Transformers\GetReservedMachinesByDateTransformer;
use App\Http\Responses\Transformers\MachineReservationTransformer;
use App\Models\Machine;
use App\Models\MachineReservation;
use App\Repositories\MachineRepository;
use EllipseSynergie\ApiResponse\Contracts\Response as ApiResponse;
use Illuminate\Http\Response;

/**
 * Class MachineController
 * @package App\Http\Controllers
 */
class MachineController extends Controller
{
    /**
     * @var ApiResponse
     */
    protected $response;

    /**
     * MachineController constructor.
     * @param ApiResponse $response
     */
    public function __construct(ApiResponse $response)
    {
        $this->response = $response;
    }

    /**
     * @param ReserveMachineRequest $request
     * @param MachineRepository $machineRepository
     * @param Machine $machine
     * @param MachineReservation $machineReservation
     *
     * @return Response
     * @throws MachineIsAlreadyReservedException
     */
    public function reservation(
        ReserveMachineRequest $request,
        MachineRepository $machineRepository,
        Machine $machine,
        MachineReservation $machineReservation
    )
    {
        $machineRepository->checkIfMachineIsAvailableAtDay($machine, $request->get('day_of_week'));

        $machineReservation->start_time = $request->get('start_time');
        $machineReservation->end_time = $request->get('start_time');
        $machineReservation->day_of_week = $request->get('day_of_week');

        return $this->response->withItem(
            $machine->reservations()->save($machineReservation),
            new MachineReservationTransformer()
        );
    }

    /**
     * @param MachineRepository $machineRepository
     * @param GetReservedMachinesByDateRequest $request
     *
     * @return Response
     */
    public function getReserved(MachineRepository $machineRepository, GetReservedMachinesByDateRequest $request)
    {
        $startDate = $request->get('start_date');
        $days = $request->get('days');

        $inDays = DateHelper::getArrayOfDOWFromDateForDays($startDate, $days);

        $machines = $machineRepository->getReservedInDays($inDays);
        return $this->response->withItem($machines, new GetReservedMachinesByDateTransformer($startDate, $days));
    }
}

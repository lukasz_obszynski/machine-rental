.PHONY: first-run run stop composer db-migrate db-seed hard-reset

composer-install:
	docker-compose run composer

run:
	docker-compose up -d

stop:
	docker-compose down

db-migrate:
	docker-compose exec php php artisan migrate

db-seed:
	docker-compose exec php php artisan migrate

first-run:
	cp .env.example .env
	make run
	make composer-install
	make db-migrate
	make db-seed

hard-reset:
	docker-compose rm -f
	make first-run

<?php

use App\Http\Controllers\MachineController;

Route::get('machines/reserved', MachineController::class . '@getReserved');
Route::post('machine/{machine}/reservation', MachineController::class . '@reservation');

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class MachineSeeder
 */
class MachineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('machine')->insert([
            ['id' => 1, 'name' => 'Excavator 011'],
            ['id' => 2, 'name' => 'Excavator 012'],
            ['id' => 3, 'name' => 'Excavator 005'],
            ['id' => 4, 'name' => 'Tipper lorry 002'],
        ]);

        DB::table('machine_reservation')->insert([
           ['machine_id' => 1, 'day_of_week' => 2, 'start_time' => '9:00', 'end_time' => '11:00'],
           ['machine_id' => 4, 'day_of_week' => 2, 'start_time' => '12:00', 'end_time' => '13:00'],
           ['machine_id' => 1, 'day_of_week' => 3, 'start_time' => '06:00', 'end_time' => '14:00'],
           ['machine_id' => 2, 'day_of_week' => 4, 'start_time' => '06:00', 'end_time' => '10:00'],
           ['machine_id' => 3, 'day_of_week' => 4, 'start_time' => '09:45', 'end_time' => '12:00'],
        ]);
    }
}
